const problem1 = require('../problem1');
const data = require('../data');


let carId = 33;
let car = problem1(data.inventory, carId);
console.log(`Car ${carId} is a ${car.car_year} ${car.car_make} ${car.car_model}`);

carId = 46;
car = problem1(data.inventory, carId);
console.log(`Car ${carId} is a ${car.car_year} ${car.car_make} ${car.car_model}`);

carId = 'hello';
car = problem1(data.inventory, carId);
console.log(car);

carId = undefined;
car = problem1(data.inventory, carId);
console.log(car);

carId = null;
car = problem1(data.inventory, carId);
console.log(car);

car = problem1(data.inventory);
console.log(car);

carId = 12;
car = problem1(carId);
console.log(car);

carId = 12;
car = problem1(carId, carId);
console.log(car);

carId = 12;
car = problem1([], carId);
console.log(car);

carId = 12;
car = problem1();
console.log(car);