const problem6 = require('../problem6');
const data = require('../data');

let cars = ['Audi', 'BMW'];
let carsList = problem6(data.inventory, cars);
console.log(JSON.stringify(carsList));

cars = ['GMC'];
carsList = problem6(data.inventory, cars);
console.log(JSON.stringify(carsList));

cars = ['GMC', 'Audi', 'Ford',];
carsList = problem6(data.inventory, cars);
console.log(JSON.stringify(carsList));

cars = 'hello';
carsList = problem6(data.inventory, cars);
console.log(JSON.stringify(carsList));

carsList = problem6(data.inventory);
console.log(JSON.stringify(carsList));

carsList = problem6(data.inventory, []);
console.log(JSON.stringify(carsList));

cars = ['GMC'];
carsList = problem6([], cars);
console.log(JSON.stringify(carsList));

cars = ['GMC'];
carsList = problem6(undefined, cars);
console.log(JSON.stringify(carsList));