function carsOlderThanGivenYear(carYears, year){
  if(!Array.isArray(carYears) || typeof(year) !== 'number'){ 
    return [];
  }
  let olderCars = [];
  for(let i = 0; i < carYears.length; i++){
    if(carYears[i] < year){
      olderCars.push(carYears[i]);
    }
  }
  return olderCars;
}

module.exports = carsOlderThanGivenYear;
