function getSpecifiedCars(inventory, cars){
  if(!Array.isArray(inventory) || !Array.isArray(cars)){
    return [];
  }
  let carsSet = new Set(cars);
  let carArray = [];
  for(let i = 0; i < inventory.length; i++){
    if(carsSet.has(inventory[i].car_make)){
      carArray.push(inventory[i]);
    }
  }
  return carArray;
}

module.exports = getSpecifiedCars;
